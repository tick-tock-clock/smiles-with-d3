import { Face } from './Face';
import { range } from 'd3';

const width = 160;
const height = 160;

const faces = range(18);

export const App = () =>
  faces.map(() => (
    <Face
      width={width}
      height={height}
      centerX={width / 2}
      centerY={height / 2}
      strokeWidth={10}
      eyeOffsetX={20 + Math.random() * 9}
      eyeOffsetY={20 + Math.random() * 9}
      eyeRadius={5 + Math.random() * 10}
      mouthWidth={2.5 + Math.random() * 10}
      mouthRadius={5 + Math.random() * 10}
    />
  ));
